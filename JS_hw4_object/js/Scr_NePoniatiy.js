/*Задание
Реализовать функцию для создания объекта "пользователь". Задача должна быть реализована на языке javascript,
без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:
- Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
- При вызове функция должна спросить у вызывающего имя и фамилию.
- Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
- Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя,
  соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
- Создать пользователя с помощью функции createNewUser().
- Вызвать у пользователя функцию getLogin().
- Вывести в консоль результат выполнения функции.

Необязательное задание продвинутой сложности:
- Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую.
- Создать функции-сеттеры setFirstName() и setLastName(), которые позволят изменить данные свойства.
*/

const user = {
    set set_fName(value){
        Object.defineProperty(user, "firstName", {writable: true});
        this.firstName = value;
        this.login = this.getLogin();
        Object.defineProperty(user, "firstName", {writable: false});
    },
    set set_lName(value){
        Object.defineProperty(user, "lastName", {writable: true});
        this.lastName = value;
        this.login = this.getLogin();
        Object.defineProperty(user, "lastName", {writable: false});
    }
};
function createNewUser(fname, lname){
    const newUser = {};
    let firstName, lastName;
    if(fname) {
        while (!firstName || !isNaN(firstName)) {
            firstName = prompt('Ваше Имя');
        }
        newUser.firstName = firstName;
    }
    if(lname) {
        while (!lastName  || !isNaN(lastName)) {
            lastName = prompt('Ваша Фамилия');
        }
        newUser.lastName = lastName;
    }
    return newUser;
}

user.firstName = createNewUser(1,0).firstName;                // console.log(Object.getOwnPropertyDescriptor(user, "firstName"));
Object.defineProperty(user, "firstName", {writable: false});   // Object.defineProperty(user, "firstName", {writable: false, enumerable: true, configurable: true});
console.log('user firstName: ', user.firstName);

alert('Попрубуйте изменить имя через свойство! (не должно получится)')
user.firstName = createNewUser(1,0).firstName;
console.log('user firstName (без изменений): ', user.firstName);

user.lastName = createNewUser(0,1).lastName;
Object.defineProperty(user, "lastName", {writable: false});

user.getLogin = function(){
    return (this.firstName[0] + this.lastName).toLowerCase();
}
user.login = user.getLogin();
console.log('User after create', user);


alert('Попрубуйте изменить имя через сетер! (должно получится)')
user.set_fName = createNewUser(1,0).firstName;
user.set_lName = 'Voykshnaras';
console.log('User after reset data', user);








