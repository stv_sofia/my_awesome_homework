/*Задание
Реализовать функцию для создания объекта "пользователь". Задача должна быть реализована на языке javascript,
без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:
- Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
- При вызове функция должна спросить у вызывающего имя и фамилию.
- Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
- Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя,
  соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
- Создать пользователя с помощью функции createNewUser().
- Вызвать у пользователя функцию getLogin().
- Вывести в консоль результат выполнения функции.
*/

function getData(question) {
    let enteredText;
    do {
        enteredText = prompt(question);
    } while (!enteredText || !isNaN(enteredText));
    return enteredText;
}
function createNewUser(firstName, lastName) {
    let newUser = {
        firstName,
        lastName,
        getLogin: function () {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
    };
    return newUser;
}
const user = createNewUser(getData('Введите имя'), getData('Введите фамилию'));
console.log(user);
console.log(user.getLogin());










// function getFirstNameUser() {
//     let fName;
//     while (true) {
//         fName = prompt('Ваше Имя');
//         if (fName && isNaN(fName) && fName !== 0)
//             return fName;
//     }
// }
// function getLastNameUser() {
//     let lName = '';
//     while (true) {
//         lName = prompt('Ваша Фамилия');
//         if (lName && isNaN(lName) && lName !== 0)
//             return lName;
//     }
// }
//
// function CreateNewUser() {
//     let newUser = {
//         firstNameU : getFirstNameUser(),
//         lastNameU : getLastNameUser(),
//         getLoginU : function () {
//             return (this.firstNameU[0] + this.lastNameU).toLowerCase();
//         }
//         // ,
//         // loginU : this.getLoginU()  - почему не работает?
//     }
//     return newUser;
// }
// const user = CreateNewUser();
