/*Задание
Реализовать функцию для создания объекта "пользователь". Задача должна быть реализована на языке javascript,
без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:
- Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
- При вызове функция должна спросить у вызывающего имя и фамилию.
- Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
- Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя,
  соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
- Создать пользователя с помощью функции createNewUser().
- Вызвать у пользователя функцию getLogin().
- Вывести в консоль результат выполнения функции.

Необязательное задание продвинутой сложности:
- Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую.
- Создать функции-сеттеры setFirstName() и setLastName(), которые позволят изменить данные свойства.
*/
function getUserName(question) {
    let enteredText;
    do {
        enteredText = prompt(question);
    } while (!enteredText || !isNaN(enteredText));
    return enteredText;
}

function createNewUser(firstName, lastName){
    let login = (firstName[0] + lastName).toLowerCase();
    let newUser = {
        set_fName: function (fName) {
            Object.defineProperty(this, 'firstName', {
                value: fName
            })
            Object.defineProperty(this, 'login', {
                value: (fName[0] + this.lastName).toLowerCase()
            })
        },
        set_lName: function (lName) {
            Object.defineProperty(this, 'lastName', {             // Object.defineProperty(this, "lastName", {writable: true}); // this.lastName = lName; // this.Login = (this.firstName[0] + lName).toLowerCase(); // Object.defineProperty(user, "lastName", {writable: false});
                value: lName
            })
            Object.defineProperty(this, 'login', {
                value: (this.firstName[0] + lName).toLowerCase()
            })
        }
    };

    Object.defineProperties(newUser, {
        firstName: {
            value: firstName,
            writable: false,
            configurable: true
        },
        lastName: {
            value: lastName,
            writable: false,
            configurable: true
        },
        login :{
            value: login,
            writable: false,
            configurable: true
        }
    });

    return newUser;
}
const user = createNewUser(getUserName('Your name:'), getUserName('Your last name:'));
console.log('User just after create', user);

user.set_fName('Viktoriya');
user.set_lName('Voykshnaras');
console.log('User after reset first and last name from script', user);

user.set_fName(getUserName('Your name:'));
user.set_lName(getUserName('Your last name:'));
console.log('User after reset first and last name from prompt', user);



































