document.addEventListener('DOMContentLoaded', onReady);
function onReady() {
    document.querySelector('.answer').insertAdjacentHTML('afterbegin', `<table class="table table-black-selected"></table>`);
    let table = document.querySelector('.table');
    let trTableStr = '';
    for(let i = 0; i < 30; i++){
        trTableStr += '<tr>';
        for(let i = 0; i < 30; i++){
            trTableStr += '<td></td>';
        }
        trTableStr += '</tr>';
    }
    table.insertAdjacentHTML('afterbegin', trTableStr);

    document.body.addEventListener('click', function (event){
        if(table.contains(event.target)) return;
        exchangeSelectColor();
    });

    table.addEventListener('click', function (event){
        let targetTD = event.target;
        if (!targetTD) return;
        if (!table.contains(targetTD)) return;
        colorSelectedTD(targetTD);
    });

    function colorSelectedTD(td) {
        let selectedTD = td;
        if(table.classList.contains('table-black-selected')){
            if(selectedTD.classList.contains('black')){
                selectedTD.classList.toggle('black');
            } else {
                selectedTD.classList.add('black');
            }
        } else {
            if(selectedTD.classList.contains('white')) {
                selectedTD.classList.toggle('white');
            }else {
                selectedTD.classList.add('white');
            }
        }

    }
    function exchangeSelectColor() {
        if(table.classList.contains('table-black-selected')){
            table.classList.remove('table-black-selected');
            table.classList.add('table-white-selected');

            const selected = document.querySelectorAll('.black');
            selected.forEach(element => {
                element.classList.remove('black');
                element.classList.add('white');
            });
        } else if (table.classList.contains('table-white-selected')){
            table.classList.remove('table-white-selected');
            table.classList.add('table-black-selected');

            const selected = document.querySelectorAll('.white');
            selected.forEach(element => {// alert('cvtyfffff');
                element.classList.remove('white');
                element.classList.add('black');
            });
        }
    }

}