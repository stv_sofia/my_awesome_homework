document.addEventListener('DOMContentLoaded', onReady);
function onReady() {
    let table = document.getElementById('table');

    document.body.addEventListener('click', function (event) {      // event.stopPropagation();
        if(table.contains(event.target)) return;    // console.log(event.target);
        exchangeSelectColor();
    });

    table.addEventListener('click', function (event) {
        let targetTD = event.target;    // let targetTD = event.target.closest('td');
        if (!targetTD) return;
        if (!table.contains(targetTD)) return;
        colorSelectedTD(targetTD);
    });

    function colorSelectedTD(td) {
        let selectedTD = td;
        if(table.classList.contains('table-black-selected')){
            if(selectedTD.classList.contains('black')){
                selectedTD.classList.toggle('black');
            } else {
                selectedTD.classList.add('black');
            }
        } else {
            if(selectedTD.classList.contains('white')) {
                selectedTD.classList.toggle('white');
            }else {
                selectedTD.classList.add('white');
            }
        }

    }
    function exchangeSelectColor() {
        if(table.classList.contains('table-black-selected')){
            table.classList.remove('table-black-selected');
            table.classList.add('table-white-selected');

            const selected = document.querySelectorAll('.black');    // console.log('selected:  >> ', selected);  // console.log(selected[0].className.setAttribute('background', 'black'));
            selected.forEach(element => {  // alert('changing');
                element.classList.remove('black');
                element.classList.add('white');
            });
        } else if (table.classList.contains('table-white-selected')){
            table.classList.remove('table-white-selected');
            table.classList.add('table-black-selected');

            const selected = document.querySelectorAll('.white');
            selected.forEach(element => {// alert('cvtyfffff');
                element.classList.remove('white');
                element.classList.add('black');
            });
        }
    }
}





// let td = document.getElementsByTagName('td');
// if(!td.classList){
//     console.log('no class');
// for(let elem of td){
//     elem.classList.add('white');
// }}



// console.log(document.getElementsByClassName('black')[0].getAttribute('class'));