// Задание
// Реализовать возможность смены цветовой темы сайта пользователем. Задача должна быть реализована на языке javascript,
// без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
// — Взять любое готовое домашнее задание по HTML/CSS.
// — Добавить на макете кнопку "Сменить тему".
// — При нажатии на кнопку - менять цветовую гамму сайта (цвета кнопок, фона и т.д.) на ваше усмотрение.
// — При повтором нажатии - возвращать все как было изначально - как будто для страницы доступны две цветовых темы.
// — Выбранная тема должна сохраняться и после перезагрузки страницы
document.addEventListener('DOMContentLoaded', onReady);
function onReady() {
    const bntChangeStyle = document.querySelector('#changeSiteStyle');
    const container = document.querySelector(".container");
    const centerLogo = document.getElementById('centerLogoImg');
    const upLogo = document.querySelector('.logo-header');

    if(localStorage.getItem('bgcolor') !== null){
        container.style.background = localStorage.getItem('bgcolor');
    }
    if(localStorage.getItem('txtColor') !== null){
        container.style.color = localStorage.getItem('txtColor');
    }
    if(localStorage.getItem('centrLogoSrc') !== null){
        centerLogo.setAttribute('src',`${localStorage.getItem('centrLogoSrc')}`);
    }
    if(localStorage.getItem('upLogoColor') !== null){
        upLogo.style.color = localStorage.getItem('upLogoColor');
    }

    container.addEventListener("click", function (event) {    // console.log(event.target);
        if (event.target === bntChangeStyle){
            if(bntChangeStyle.classList.contains('black')){
                bntChangeStyle.classList.remove('black');
                container.style.background = 'white';
                localStorage.setItem('bgcolor', 'white');
                container.style.color = 'black';
                localStorage.setItem('txtColor', 'black');
                centerLogo.setAttribute('src','img/top1-img 1.png');
                localStorage.setItem('centrLogoSrc', "img/top1-img 1.png");
                upLogo.style.color = 'black';
                localStorage.setItem('upLogoColor', 'black');
            } else {
                bntChangeStyle.classList.add('black');
                container.style.background = 'black';
                localStorage.setItem('bgcolor', 'black');
                container.style.color = 'white';
                localStorage.setItem('txtColor', 'white');
                centerLogo.setAttribute('src','img/top1-img 1white.png');
                localStorage.setItem('centrLogoSrc', "img/top1-img 1white.png");
                upLogo.style.color = 'white';
                localStorage.setItem('upLogoColor', 'white');
            }
        }
    });
}