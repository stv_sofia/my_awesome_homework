// Задание
// Реализовать возможность смены цветовой темы сайта пользователем. Задача должна быть реализована на языке javascript,
// без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
// — Взять любое готовое домашнее задание по HTML/CSS.
// — Добавить на макете кнопку "Сменить тему".
// — При нажатии на кнопку - менять цветовую гамму сайта (цвета кнопок, фона и т.д.) на ваше усмотрение.
// — При повтором нажатии - возвращать все как было изначально - как будто для страницы доступны две цветовых темы.
// — Выбранная тема должна сохраняться и после перезагрузки страницы
document.addEventListener('DOMContentLoaded', onReady);
function onReady() {
    const bntChangeStyle = document.querySelector('#changeSiteStyle');
    const container = document.querySelector(".container");
    const centerLogo = document.getElementById('centerLogoImg');
    const upLogo = document.querySelector('.logo-header');

    if(localStorage.getItem('darkStyle') !== null){
        // Storage.clear();
        bntChangeStyle.classList.add('dark-style');
        container.classList.add('black');
        upLogo.classList.add('white-txt');
        centerLogo.setAttribute('src','img/top1-img 1white.png');
        // localStorage.setItem('darkStyle', 'yes');
    }

    container.addEventListener("click", function (event) {    // console.log(event.target);
        if (event.target === bntChangeStyle){
            if(bntChangeStyle.classList.contains('dark-style')){
                bntChangeStyle.classList.remove('dark-style');
                container.classList.remove('black');
                upLogo.classList.remove('white-txt');
                centerLogo.setAttribute('src','img/top1-img 1.png');
                localStorage.removeItem('darkStyle');

            } else {
                bntChangeStyle.classList.add('dark-style');
                container.classList.add('black');
                upLogo.classList.add('white-txt');
                centerLogo.setAttribute('src','img/top1-img 1white.png');
                localStorage.setItem('darkStyle', 'yes');
            }
        }
    });
}