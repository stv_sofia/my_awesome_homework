// Написать программу "Я тебя по айпи вычислю"
//
// Технические требования:
//
// Создать простую HTML страницу с кнопкой Вычислить по IP.
// По нажатию на кнопку - отправить AJAX запрос по адресу https://api.ipify.org/?format=json,
// получить оттуда IP адрес клиента.
// Узнав IP адрес, отправить запрос на сервис https://ip-api.com/ и получить информацию
// о физическом адресе.
// Под кнопкой вывести на страницу информацию, полученную из последнего запроса
// - континент, страна, регион, город, район города.
// Все запросы на сервер необходимо выполнить с помощью async await.
//
// Примечание
// Задание должно быть выполнено на "чистом" Javascript без использования библиотек
// типа jQuery или React.
//
// Литература
// Async/await
// async function
// Документация сервиса ip-api.com

document.addEventListener('DOMContentLoaded', function () {
    document.querySelector('#btn').addEventListener('click', onBtnClick);

    class ServerRequest {
        static async getData(url) {
            const response = await fetch(url);   // console.log(response);
            return response.json();
        }
    }

    function printDataToDOM(data, ip) {
        return `<ul><h3>Геолокация по IP адресу ${ip}:</h3> ${Object.entries(data)
            .map( ([key, value]) => `<li>${key}: ${value}</li>`)
            .join('')}</ul>`;

        // return `<ul><h3>Геолокация по IP адресу ${ip}:</h3>
        //     <li>Континент: ${data.continent}</li>
        //     <li>Cтрана: ${data.country}</li>
        //     <li>Pегион: ${data.region}</li>
        //     <li>Название региона: ${data.regionName}</li>
        //     <li>Город: ${data.city}</li>
        //     <li>Почтовый индекс: ${data.zip}</li>
        // </ul>`;
    }

    async function onBtnClick () {
        const {ip} = await ServerRequest.getData('https://api.ipify.org/?format=json');
        console.log(ip);

        // const data = await ServerRequest.getRequest(`http://ip-api.com/json/${ip}?lang=ru&fields=668467191`);
        const data = await ServerRequest.getData(`http://ip-api.com/json/${ip}?fields=continent,country,region,regionName,city,zip,query&lang=ru`)
        console.log(data);

        document.querySelector("#root").insertAdjacentHTML("beforeend", printDataToDOM(data, ip));
    }
});