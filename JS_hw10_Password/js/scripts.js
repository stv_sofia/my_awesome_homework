document.addEventListener('DOMContentLoaded', onReady);
function onReady(){
    // const showPswBtn = document.querySelectorAll('.icon-password');
    // showPswBtn.item(0).addEventListener("click", () => showPsw(showPswBtn.item(0)));
    // showPswBtn.item(1).addEventListener("click", () => showPsw(showPswBtn.item(1)));   // console.log(showPswBtn.item(1));

    const form = document.querySelector(".password-form");
    form.addEventListener("click", function (event) {
        let targetShowPswBtn = event.target;
        if(targetShowPswBtn.tagName === "I")
            showPsw(targetShowPswBtn);
    });

    function showPsw(showPswBtn) {
        if (showPswBtn.classList.contains('fa-eye')){
            showPswBtn.classList.remove('fa-eye');
            showPswBtn.classList.add('fa-eye-slash');    // console.log(showPswBtn.classList);  // let ddd = document.getElementById(`${showPswBtn.id}_eye`);  // console.log(ddd);
            document.getElementById(`${showPswBtn.id}_eye`).setAttribute('type', 'text');
        } else {
            if (showPswBtn.classList.contains('fa-eye-slash')){
                showPswBtn.classList.remove('fa-eye-slash');
                showPswBtn.classList.add('fa-eye');      // console.log(showPswBtn.classList);
                document.getElementById(`${showPswBtn.id}_eye`).setAttribute('type', 'password');
            }
        }
    }

    document.querySelector('.btn').onclick = function(e) {
        e.preventDefault();
        if (document.getElementById('eye_eye').value === '' && document.getElementById('slash_eye').value === ''){     // console.log(document.getElementById('slash_eye').value);
            alert('Enter password fields!');
            return;
        }
        if (document.getElementById('eye_eye').value === document.getElementById('slash_eye').value) {
            if(document.getElementById('notMatch')){
                document.getElementById('notMatch').remove();
            }
            alert('You are welcome!');
            document.getElementById('eye_eye').value = '';
            document.getElementById('slash_eye').value = '';
        } else {
            if(!document.getElementById('notMatch')) {
                document.querySelector('.confirmation').insertAdjacentHTML('afterbegin',
                    `<p id="notMatch" class="not-match">Password entry fields must match the same!</p>`);
            }
        }
    };

}
