// Задание
// Получить список фильмов серии Звездные войны, и вывести на экран список персонажей для каждого из них.
//
// Технические требования:
//
// Отправить AJAX запрос по адресу https://swapi.dev/api/films/ и получить список всех фильмов серии Звездные войны
//
// Для каждого фильма получить с сервера список персонажей, которые были показаны в данном фильме. Список персонажей можно получить из свойства characters.
// Как только с сервера будет получена информация о фильмах, сразу же вывести список всех фильмов на экран. Необходимо указать номер эпизода, название фильма, а также короткое содержание (поля episode_id, title и opening_crawl).
// Как только с сервера будет получена информация о персонажах какого-либо фильма, вывести эту информацию на экран под названием фильма.
//
//
// Необязательное задание продвинутой сложности
//
// Пока загружаются персонажи фильма, прокручивать под именем фильма анимацию загрузки. Анимацию можно использовать любую. Желательно найти вариант на чистом CSS без использования JavaScript.
//
//
// Примечание
// Задание должно быть выполнено на "чистом" Javascript без использования библиотек типа jQuery или React.
//
// Литература:
//
// Использование Fetch на MDN
// Fetch
// CSS анимация
// События DOM

async function sendQueryGetData(url) {
    const response = await fetch(url);
    return response.json();
}

async function getFilms() {
    return await sendQueryGetData('https://swapi.dev/api/films/');
}

getFilms()
    .then( ({results}) => results )
    .then(films => {
        const ul = document.createElement('ul');   // Ul_For_Film_Item
        document.querySelector('#root').append(ul);

        films.forEach( ({episode_id, title, opening_crawl, characters}) => {
                const li = document.createElement('li');     // Li_For_Film_Item_details
                li.innerHTML = `<h3>Title: ${title} ||
                                Episode: ${episode_id} </h3>
                                <p><strong>Opening crawl:</strong> ${opening_crawl}</p>`;
                ul.appendChild(li);

                Promise
                    .all( characters.map(url => sendQueryGetData(url)) )
                    .then( (charactersList) => {
                        const UlForCharacters = document.createElement('ul');            // console.log(charactersList);
                        UlForCharacters.innerHTML = `<strong>Characters</strong> of "${title}": `

                        charactersList.forEach( ({name, birth_year, gender}) => {
                            const LiForCharacters = document.createElement('li');
                            LiForCharacters.innerHTML = `name: ${name} || birth_year: ${birth_year} || gender: ${gender}`;
                            UlForCharacters.appendChild(LiForCharacters);
                        });

                        li.appendChild(UlForCharacters);
                    });
            });
    });