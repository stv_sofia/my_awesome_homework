// Задание
// Получить список фильмов серии Звездные войны, и вывести на экран список персонажей для каждого из них.
//
// Технические требования:
//
// Отправить AJAX запрос по адресу https://swapi.dev/api/films/ и получить список всех фильмов серии Звездные войны
//
// Для каждого фильма получить с сервера список персонажей, которые были показаны в данном фильме. Список персонажей можно получить из свойства characters.
// Как только с сервера будет получена информация о фильмах, сразу же вывести список всех фильмов на экран. Необходимо указать номер эпизода, название фильма, а также короткое содержание (поля episode_id, title и opening_crawl).
// Как только с сервера будет получена информация о персонажах какого-либо фильма, вывести эту информацию на экран под названием фильма.
//
//
// Необязательное задание продвинутой сложности
//
// Пока загружаются персонажи фильма, прокручивать под именем фильма анимацию загрузки. Анимацию можно использовать любую. Желательно найти вариант на чистом CSS без использования JavaScript.
//
//
// Примечание
// Задание должно быть выполнено на "чистом" Javascript без использования библиотек типа jQuery или React.
//
// Литература:
//
// Использование Fetch на MDN
// Fetch
// CSS анимация
// События DOM

const filmsURL = 'https://swapi.dev/api/films/';

function sendRequest(method, url, bool) {
    return new Promise( (resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.open(method, url, bool = true);

        xhr.responseType = 'json';

        xhr.onload = () => {
            if (xhr.status >= 400) {
                reject(xhr.response);
            } else {
                resolve(xhr.response);
            }
        }
        xhr.send();
    });
}

const dataFilmsArray = sendRequest('GET', filmsURL)
    .then((data) => {                                                        // console.log(data))
        console.log(data.results);
        return data.results;
    })
    .catch(err => console.log(err))

class Films {
    constructor(films) {
        this.films = films;
    }
    render () {
        this.films.map( ({episode_id, title, opening_crawl, characters}) => {
            const container = document.createElement('div'),
                  h2 = document.createElement('h2'),         // h2.className = 'title';
                  p = document.createElement('p');
            h2.textContent = `Episode >>>  ${episode_id} || Title >>>  "${title}"`;
            p.textContent = `Opening crawl >>> ${opening_crawl}`;

            container.append(h2, p);
            document.querySelector('#root').append(container);

            const ul = document.createElement('ul');
            ul.textContent = `Characters of "${title}" : `;
            characters.forEach((el, id) => {
                sendRequest('GET', el, false)
                    .then((data) => {                                           // console.log(data);
                        let li = document.createElement('li')
                        li.textContent = `id: ${id+1} || name: ${data.name} || birth_year: ${data.birth_year} || gender: ${data.gender}`;
                        ul.append(li)    // id вывожу чтоб посмотреть кто когда ответит)) кто раньше т е)
                    })
                container.appendChild(ul).after(p);
            })
        });
    }
}

document.addEventListener('DOMContentLoaded', function () {
    dataFilmsArray
        .then( (data) => { new Films(data).render() } );  // new Films(dataFilmsArray).render(); - так не работает
});
