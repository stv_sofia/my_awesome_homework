// Задание
// Создать объект студент "студент" и проанализировать его табель. Задача должна быть реализована на языке javascript,
// без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
// Создать пустой объект student, с полями name и last name.
//  - Спросить у пользователя имя и фамилию студента, полученные значения записать в соответствующие поля объекта.
//  - В цикле спрашивать у пользователя название предмета и оценку по нему.
//           Если пользователь нажмет Cancel при n-вопросе о названии предмета, закончить цикл.
//  - Записать оценки по всем предметам в свойство студента tabel.
//  - Посчитать количество плохих (меньше 4) оценок по предметам.
//          Если таких нет, вывести сообщение Студент переведен на следующий курс.
//  - Посчитать средний балл по предметам.
//          Если он больше 7 - вывести сообщение Студенту назначена стипендия.
//
// Литература:
// Объекты как ассоциативные массивы
// Перебор свойство объектов

function GetStudentData(questTxt, questGrade) {
    let enterdTxt;
    if (questGrade){
        do{
            enterdTxt = prompt(questGrade);
        } while (!Boolean(+enterdTxt) || +enterdTxt < 0 || +enterdTxt > 12);
    } else {
        do {
            enterdTxt = prompt(questTxt);
            if (enterdTxt === null){return enterdTxt;}
        } while (!enterdTxt || !isNaN(enterdTxt));
    }
    return enterdTxt;
}
function SetTable(course, courseGrade) {
    let table = {};
    do {
        table[course] = courseGrade;
        course = GetStudentData('Введите название предмета');
        !course ? null : courseGrade = GetStudentData('вопрос1',  'Оценка');
    } while (course);
    return table;
}
function CreateNewStudent(firstName, lastName) {
    return {
        firstName,
        lastName,
        setTable : function (){
            return SetTable(GetStudentData('Введите название предмета'), GetStudentData('вопрос1',  'Оценка'));
        }
    };
}
function CheckGrade(student) {
    let lowLimitCount = 0, sredneye = 0, count = 0;
    for(let key in student.table){
        student.table[key] <= 4 ? lowLimitCount++ : lowLimitCount;
        sredneye += +student.table[key];
        count ++;
    }
    lowLimitCount === 0 ? console.log(`Студент ${student.lastName} переведен на следующий курс`) : console.log(`У студента ${student.lastName} недопустимо низкие оценки по ${lowLimitCount} предметам`);
    sredneye/count >= 7 ? console.log(`Студенту ${student.lastName} назначена стипендия`) : console.log(`У студентa ${student.lastName} средний бал: ${sredneye/count}`) ;
}
const student = CreateNewStudent (GetStudentData('Введите имя'), GetStudentData('Введите фамилию'));
console.log(student);
console.log(student.table = student.setTable());
CheckGrade(student);