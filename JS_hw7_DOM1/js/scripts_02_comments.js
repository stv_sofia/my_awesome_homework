// Задание
// Реализовать функцию, которая будет получать массив элементов
// и выводить их на страницу в виде списка. Задача должна быть реализована на языке javascript,
// без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
//      - Создать функцию, которая будет принимать на вход массив.
//      - Каждый из элементов массива вывести на страницу в виде пункта списка
//      - Необходимо использовать шаблонные строки и функцию map массива для формирования контента списка перед выведением его на страницу.
//
//      Примеры массивов, которые можно выводить на экран:
//      ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']
//      ['1', '2', '3', 'sea', 'user', 23]         Можно взять любой другой массив.
//
// Необязательное задание продвинутой сложности:
//      - Очистить страницу через 10 секунд. Показывать таймер обратного отсчета (только секунды) перед очищением страницы.
//      - Если внутри массива одним из элементов будет еще один массив или объект, выводить его как вложенный список.
//
// Литература:
// Поиск DOM элементов
// Добавление и удаление узлов
// Шаблонные строки
// Array.prototype.map()
const rootElem = document.getElementById('menu_div');      // буду использовать не всю страницу для вывода, а родитель-элемент в DOM (div)
let array = [['Hello', 'Hello'], 'World', ['Kiev', 'Kiev', 'Kiev', 'Kiev'], 'Kharkiv', 'Odessa', 'Lviv'];     // можно получить из вне
let timerSec = 10;     // можно получить из вне

const buildListButton = document.getElementById('buildListButton');  // достаем кнопку для создания списка из DOM по id
buildListButton.onclick = () => createList(array, rootElem, timerSec);     // вызываем функцию на событие onclick по кнопку

function createListLi(root, content) {
    let li = document.createElement('li');
    li.innerHTML = content;
    root.append(li);
}

function createList(array, rootElem, timerSec){   // функция создания списка принимает родитель-элемент в DOM (div), массив, время в сек (через когда все ИСЧЕЗНЕТ - МАГИЯ!))
    buildListButton.classList.add('hidden');   // прячем кнопку для создания списка

    let ulParent = document.createElement('ul');  // создаем список-родитель
    ulParent.classList.add('list-style');     // добавляем списку-родитель класс с display: inline-flex; - в строку список хочу))

    setTimer(rootElem, timerSec);   // внедряем таймер - перед (выводом) списком хочу)) // передаем в ф-ию таймер родитель-элемент в DOM (div) - где и будет происходить все это безобразие (вывод на стр т е)

    for(let elem of array){       //  перебор всех elem массива array
        let ul = document.createElement('ul');     // создаем новый список UL
            if (Array.isArray(elem)) {     // если elem массива Оказался сам Массивом - ВСЕ ПРОПАЛО!))
                for(let subItem of elem) {    // перебираем его эл-ты subItem
                    createListLi(ul, subItem);  // создаем для каждого subItem пункт списка li
                }
            } else {        // если elem массива НЕ МАССИВ, а нормальный elem))) то -->
                createListLi(ul, elem);  // создаем для elem 1 пункт списка
            }
        ulParent.append(ul);  // добавляем на стр один UL - кот соответствует одному эл-ту массива array
    }

    rootElem.append(ulParent);    // проявляем (добавляем на стр) родительский список в родителе-элементе div

    //  через 10 сек удаляем все что насоздавали из родителя-элемента div
    let idInt = setTimeout(() =>rootElem.removeChild(ulParent), (timerSec+1)*1000);  // clearTimeout(idInt); ? нужно или нет ? - не поняла..
}

function setTimer(rootElem, i) {
    let btnTimer = document.createElement('button');    // создаем кнопку таймера
    btnTimer.classList.toggle('button');
    btnTimer.classList.add('timer');    // с классами стилей балумся
    btnTimer.innerHTML = i;            // в значение кнопки заносим timerSec - ф-ия принимает при вызове
    rootElem.append(btnTimer);            // проявляем
    let idInt = setInterval(function() {
        if (i < 0 || i > 10 ) return;      // диапазон 0:10 если вне диапазона - тихо уходим
        i--;                              // меняем значение на уменьшение
        // console.log(i);    // это так - для отладки)
        (i < 10 && i >= 0) ? btnTimer.innerHTML = `0${i}` : clearInterval(idInt);   // если в диапазоне - заносим значение в кнопку, нет - очищаем - clearInterval(idInt);
    }, 1000);
    setTimeout(() =>rootElem.removeChild(btnTimer), (i+1)*1000);    // саму кнопку таймера удаляем по истечению заданного времени + 1 сек для красоты))
}

