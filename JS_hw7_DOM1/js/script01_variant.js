// Задание
// Реализовать функцию, которая будет получать массив элементов
// и выводить их на страницу в виде списка. Задача должна быть реализована на языке javascript,
// без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
//      - Создать функцию, которая будет принимать на вход массив.
//      - Каждый из элементов массива вывести на страницу в виде пункта списка
//      - Необходимо использовать шаблонные строки и функцию map массива для формирования контента списка перед выведением его на страницу.
//
//      Примеры массивов, которые можно выводить на экран:
//      ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']
//      ['1', '2', '3', 'sea', 'user', 23]         Можно взять любой другой массив.
let array = [['Hello', 'Hello', 'Hello', 'Hello'], 'World', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
let listContainer = document.getElementById('div_list_container');
function createListLi(root, content) {
        let li = document.createElement('li');
        li.innerHTML = content;
        root.append(li);
}
function createListUl(container, arr) {
    let rootUl = document.createElement('ul');
    rootUl.classList.add('list-style');
        for(let item of arr){
            let ul = document.createElement('ul');
                if(Array.isArray(item)){
                    for(let subItem of item) {
                        createListLi(ul, subItem);
                    }
                } else {
                    createListLi(ul, item);
                }
            rootUl.append(ul);
        }
    container.append(rootUl);
}
buildListButton.onclick = () => createListUl(listContainer, array);
// createListUl(listContainer, array); // ф-ия сработает без события























// let array = [['Hello', 'Hello', 'Hello', 'Hello'], 'World', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
//
//
// let listContainer = document.getElementById('div_list_container');
//
//
// function createListLi(root, content, numb) {
//     let li = document.createElement('li');
//     if(numb === 0) {
//         li.innerHTML = content.toUpperCase();
//     } else {
//         li.innerHTML = content;
//     }
//     root.append(li);
// }
//
// function createListUl(container, arr) {
//     for(let item of arr){
//         let ul = document.createElement('ul');
//         if(Array.isArray(item)){
//             let id = 0;
//             for(let subItem of item) {
//                 createListLi(ul, subItem, id);
//                 id++;
//             }
//         } else {
//             createListLi(ul, item,0);
//         }
//         container.append(ul);
//     }
// }
//
// createListUl(listContainer, array); // ф-ия сработала без события