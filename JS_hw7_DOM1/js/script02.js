// Задание
// Реализовать функцию, которая будет получать массив элементов
// и выводить их на страницу в виде списка. Задача должна быть реализована на языке javascript,
// без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
//      - Создать функцию, которая будет принимать на вход массив.
//      - Каждый из элементов массива вывести на страницу в виде пункта списка
//      - Необходимо использовать шаблонные строки и функцию map массива для формирования контента списка перед выведением его на страницу.
//
//      Примеры массивов, которые можно выводить на экран:
//      ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']
//      ['1', '2', '3', 'sea', 'user', 23]         Можно взять любой другой массив.
//
// Необязательное задание продвинутой сложности:
//      - Очистить страницу через 10 секунд. Показывать таймер обратного отсчета (только секунды) перед очищением страницы.
//      - Если внутри массива одним из элементов будет еще один массив или объект, выводить его как вложенный список.
//
// Литература:
// Поиск DOM элементов
// Добавление и удаление узлов
// Шаблонные строки
// Array.prototype.map()
const rootElem = document.getElementById('menu_div');
let array = [['Hello', 'Hello'], 'World', ['Kiev', 'Kiev', 'Kiev', 'Kiev'], 'Kharkiv', 'Odessa', 'Lviv'];
let timerSec = 10;

const buildListButton = document.getElementById('buildListButton');
buildListButton.onclick = () => createList(array, rootElem, timerSec);

function createListLi(root, content) {
    let li = document.createElement('li');
    li.innerHTML = content;
    root.append(li);
}
function createList(array, rootElem, timerSec){
    buildListButton.classList.add('hidden');
    setTimer(rootElem, timerSec);   // внедряем таймер
        let ulParent = document.createElement('ul');
        ulParent.classList.add('list-style');
            for(let elem of array){
                let ul = document.createElement('ul');
                if (Array.isArray(elem)) {
                    for(let subItem of elem) {
                        createListLi(ul, subItem);
                    }
                } else {
                    createListLi(ul, elem);
                }
                ulParent.append(ul);
            }
        rootElem.append(ulParent);
    setTimeout(() => rootElem.removeChild(ulParent), (timerSec+1)*1000);   //  через 10 сек удаляем все что насоздавали из родителя-элемента div + 1 сек для красоты))
}
function setTimer(rootElem, i) {
    let btnTimer = document.createElement('button');
    btnTimer.classList.toggle('button');
    btnTimer.classList.add('timer');
    btnTimer.innerHTML = i;            // в значение кнопки заносим timerSec - ф-ия принимает при вызове
    rootElem.append(btnTimer);
    let idInt = setInterval(function() {
        if (i < 0 || i > 10 ) return;
        i--;
        (i < 10 && i >= 0) ? btnTimer.innerHTML = `0${i}` : clearInterval(idInt);
    }, 1000);
    setTimeout(() =>rootElem.removeChild(btnTimer), (i+1)*1000);    // саму кнопку таймера удаляем по истечению заданного времени + 1 сек для красоты))
}

