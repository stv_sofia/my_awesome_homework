// Задание
// Реализовать функцию, которая будет получать массив элементов
// и выводить их на страницу в виде списка. Задача должна быть реализована на языке javascript,
// без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
//      - Создать функцию, которая будет принимать на вход массив.
//      - Каждый из элементов массива вывести на страницу в виде пункта списка
//      - Необходимо использовать шаблонные строки и функцию map массива для формирования контента списка перед выведением его на страницу.
//
//      Примеры массивов, которые можно выводить на экран:
//      ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']
//      ['1', '2', '3', 'sea', 'user', 23]         Можно взять любой другой массив.

let array = [['Hello', 'Hello', 'Hello', 'Hello'], 'World', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
function createLi(content){
    return `<li>${content}</li>`;
}
let listFromArray = array.map(function(item) {
    if(Array.isArray(item)){
        let newItem = '';
            for(let subItems of item) {
                newItem += createLi(subItems);
            }
        return `<ul>${newItem}</ul>`;
    } else {
        return `<ul>${createLi(item)}</ul>`;
    }
});     // console.log(listFromArray);
let strHtml = listFromArray.join('');   // console.log(strHtmlList);
let containerList = document.getElementById('div_list_container'); // debugger; console.log(container);
containerList.insertAdjacentHTML('afterbegin', strHtml);


// const createHtmlFromArray = (arr) => `<ul>${arr.map((el) =>Array.isArray(el) ? createHtmlFromArray(el) : `<li>${el}</li>`).join('')}</ul>`;