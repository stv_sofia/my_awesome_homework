console.log('--- Fill Employee: name -> symbols, age -> numbers, salary -> numbers ------>>');
console.log('--- Fill Programmer: lang -> anything but separated COMMA ------>>');

document.addEventListener("DOMContentLoaded", function () {
    class Employee {
        constructor(option) {
            if (typeof option._name === "undefined" || typeof option._age === "undefined" || typeof option._salary === "undefined") {
                this.set();
            } else {
                this._name = option._name;
                this._age = option._age;
                this._salary = option._salary;
            }
        }

        set() {
            do {
                this._name = prompt('Fill Employee  >>  Enter NAME   !symbols ONLY! ');
                this._age = +prompt('Fill Employee  >>  Enter age  !numbers ONLY! ');
                this._salary = +prompt('Fill Employee  >>  Enter salary  !numbers ONLY! ');
            } while (!isNaN(this._name) || !this._age || !this._salary);
        }

        get getName() {
            return this._name;
        }

        get getAge() {
            return this._age;
        }

        get getSalary() {
            return this._salary;
        }
    }

    class Programmer extends Employee {
        constructor(_name, _age, _salary) {
            super(_name, _age, _salary);
            // super({
            //     _name: option._name,
            //     _age: option._age,
            //     _salary: option._salary
            // });
            this._initialSalary = this._salary;
            this._lang = prompt('Fill Programmer  >>  Enter lang  !separated COMMA!').split(',');
            this._index = this._lang.length;
        }

        // если просто геттером возвращать this._salary * 3 (например) -
        // каждый раз вызывая programmer1.getSalary - его ЗП будет увеличиваться втрое, что конечно очень хорошо для программиста)
        // п э добавила поле начальной ставки (а поле индекса - для справедливости))
        get getSalary() {
            return this._salary = this._initialSalary * this._index;
        }
    }

// вар1 - программист на базе уже созданного сотрудника,
// т е сотрудник решил стать программистом и выучил пару языков)
// поле языков заполняется в классе Programmer и от их кол-ва происходит индексация salary)
    console.log('---------- programmer1 ------- fill data -------------------->>');
    const employee1 = new Employee({});
    console.log('employee1 data:', employee1);

    const programmer1 = new Programmer({
        _name: employee1.getName,
        _age: employee1.getAge,
        _salary: employee1.getSalary
    });
    console.log('incoming programmer1 data:', programmer1);
    console.log('new salary for programmer1 ', programmer1._initialSalary, '*', programmer1._index, ': ', programmer1.getSalary, '$');
    console.log('new programmer1 data:', programmer1);


// вар2 - программист новый сотрудник (name, age, salary(начальная ставка))
// заполняются через классе Employee и наследуются в классе Programmer,
// поле языков заполняется в классе Programmer и от их кол-ва происходит индексация salary)
    console.log('---------- programmer2 ------- fill data -------------------->>');
    const programmer2 = new Programmer({});
    console.log('incoming programmer2 data:', programmer2);
    console.log('new salary for programmer2 ', programmer2._initialSalary, '*', programmer2._index, ': ', programmer2.getSalary, '$');
    console.log('new programmer2 data:', programmer2);


// вар3 - программист новый сотрудник (name, age, salary(начальная ставка))
// передаются в параметрах // далее - аналогично
    console.log('---------- programmer3 ------- fill data -------------------->>');
    const programmer3 = new Programmer({
        _name: 'Alex',
        _age: 35,
        _salary: 1200
    });
    console.log('incoming programmer3 data:', programmer3);
    console.log('new salary for programmer3 ', programmer3._initialSalary, '*', programmer3._index, ': ', programmer3.getSalary, '$');
    console.log('new programmer3 data:', programmer3);

});