// Реализовать программу на Javascript, которая будет находить все числа кратные 5
// (делятся на 5 без остатка) в заданном диапазоне.
// Задача должна быть реализована на языке javascript,
// без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
// Считать с помощью модального окна браузера число, которое введет пользователь.
// Вывести в консоли все числа кратные 5, от 0 до введенного пользователем числа.
// Если таких чисел нету - вывести в консоль фразу `Sorry, no numbers'
// Обязательно необходимо использовать синтаксис ES6 (ES2015) при создании переменных.
//
// Необязательное задание продвинутой сложности:
// Проверить, что введенное значение является целым числом. Если данное условие не соблюдается,
// повторять вывод окна на экран до тех пор, пока не будет введено целое число.
let number = 0;
alert('----------------  TASK #1   -------------- \n ' +
    ' To Find all numbers divisible by 5 without remainder \n ' +
    ' till the given number you enter \n' +
    ' pls, Enter a natural number  // for example 127 // \n ' +
    ' pls, Enter Positive Integer numbers ONLY! ');
do {
    number = +prompt('Enter integer digit:');
    //console.log(number);
} while (!isInteger(number) || number <= 0 || isNaN(number));

function isInteger(numb) {
    return numb === parseInt(numb, 10);
}
if(number<5){
    alert('look Console for result');
    console.log('Sorry, there is no numbers divisible by 5 without remainder from 0 till', number);
} else {
    alert('Look Console for result');
    console.log('In ', number, ' the numbers divisible by 5 (x%5==0) are:');
    for (let i = number; i > 0; i--) { //console.log(i);
        if (i % 5 === 0) {    //console.log(i);
            console.log(i);
        }
    }
}
//Считать два числа, m и n. Вывести в консоль все простые числа
// в диапазоне от m до n (меньшее из введенных чисел будет m, бОльшее будет n).
// Если хотя бы одно из чисел не соблюдает условие валидации, указанное выше,
// вывести сообщение об ошибке, и спросить оба числа заново.

let numbMin = 0, numbMax = 0;
alert('----------------  TASK #2   -------------- \n' +
    ' To Find the Prime numbers in some range  // for example [2:42] // , pls \n ' +
    ' Enter Positive Integer numbers ONLY bigger then 1! \n ' +
    ' Notice: 1-st number must be smaller then the second! \n ');
do {
    numbMin = +prompt('Enter number to start with:');
    numbMax = +prompt('Enter number to end with:');
    if(isNaN(numbMin) || isNaN(numbMax) || numbMin >= numbMax || numbMin <= 1 || numbMax <= 1){
        alert('Please, enter Positive Integer numbers ONLY! Bigger then 1! ' +
            'Notice: 1-st number must be smaller then second!');
    }
} while(numbMin >= numbMax || numbMin <= 1 || numbMax <= 1|| isNaN(numbMin) || isNaN(numbMax));

function isPrime(numb) {
    for(let i=2; i<numb; i++){
        if(numb % i === 0){
            return false;
        }
    }
    return true;
}

function showPrime(numbMin, numbMax){
    alert('Look Console for result');
    console.log('From ',numbMin ,'to ',numbMax ,'prime numbers are:');
    for(let i = numbMin; i <= numbMax; i++){   // debugger;
        if(!isPrime(i)) {
            continue;
        }
        console.log(i);
    }
}
showPrime(numbMin, numbMax);






