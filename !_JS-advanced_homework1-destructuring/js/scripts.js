// ______________Задание 1____________________________________________
// У вас есть 2 массива строк, в каждом из них - фамилии клиентов.
// Создайте на их основе один массив, который будет представлять собой
// объединение двух массив без повторяющихся фамилий клиентов.
const clients1 = ["Пирс","Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон", "Пирс"];

// --- вариант 1 ---
// используею Spread (...оператор расширения) - позволяет расширить заданный массив (или другой итерируемый объект) в несколько значений.

//===== мой код =======
const uniqClients = [...new Set([...clients1, ...clients2])];
console.log('TASK 1 >>> Uniq Clients array : ' ,uniqClients);

// --- вариант 2 ---
// const uniqClients = Array.from(new Set([...clients1, ...clients2]));
// console.log('TASK 1 >>> Uniq Clients array : ' ,uniqClients);

// --- вариант 3 ---
// const uniqClients = [...clients1, ...clients2].filter((item, index) => index === [...clients1, ...clients2].indexOf(item));
// console.log('TASK 1 >>> Uniq Clients array : ' ,uniqClients);

// --- вариант 3  подробно ---
// let debug = [];
// const uniqClients = [...clients1, ...clients2].filter((item, index) => {
//     debug.push({item, index, indexOf: [...clients1, ...clients2].indexOf(item)});
//     return index === [...clients1, ...clients2].indexOf(item);
// });
// console.table(debug);
// console.log('TASK 1 >>> Uniq Clients array : ' ,uniqClients);

// --- вариант 4 ---
// const uniqClients = [...clients1, ...clients2].reduce((uniq, item) => {
//     return uniq.includes(item) ? uniq : [...uniq, item];
// }, []);
// console.log('TASK 1 >>> Uniq Clients array : ' ,uniqClients);



// ______________Задание 2____________________________________________
// Перед вами массив characters, состоящий из объектов. Каждый объект описывает одного персонажа.
// Создайте на его основе массив charactersShortInfo, состоящий из объектов,
// в которых есть только 3 поля - name, lastName и age.
const characters = [
    {
        name: "Елена",
        lastName: "Гилберт",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Кэролайн",
        lastName: "Форбс",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Аларик",
        lastName: "Зальцман",
        age: 31,
        gender: "man",
        status: "human"
    },
    {
        name: "Дэймон",
        lastName: "Сальваторе",
        age: 156,
        gender: "man",
        status: "vampire"
    },
    {
        name: "Ребекка",
        lastName: "Майклсон",
        age: 1089,
        gender: "woman",
        status: "vempire"
    },
    {
        name: "Клаус",
        lastName: "Майклсон",
        age: 1093,
        gender: "man",
        status: "vampire"
    }
];

//===== мой код =======
const charactersShortInfo = characters.map((item) => {
    let  {name, lastName, age} = ({...item});     // console.log( ({...item}.name) );
    return  { name: name, lastName: lastName, age: age };
});
console.log('TASK 2 >>> ShortInfo array of objects : ', charactersShortInfo);



// ______________Задание 3____________________________________________
// У нас есть объект user. Напишите деструктурирующее присваивание, которое:
// - свойство name присвоит в переменную name
// - свойство years присвоит в переменную age
// - свойство isAdmin присвоит в переменную isAdmin (false, если нет такого свойства)
// Выведите переменные на экран.
const user1 = { name: "John", years: 30 };

//===== мой код =======
let {name, years: age, isAdmin = false} = user1;    // alert( name ); // alert( age ); alert( isAdmin );
console.log('TASK 3 >>> Name :',name + '  / Age :',age + '  / isAdmin :',isAdmin);



// ______________Задание 4____________________________________________
// Детективное агентство несколько лет собирает информацию о возможной личности Сатоши Накамото.
// Вся информация, собранная в конкретном году, хранится в отдельном объекте.
// Всего таких объектов три - satoshi2018, satoshi2019, satoshi2020.
// Чтобы составить полную картину и профиль, вам необходимо объединить данные из этих трех объектов в один объект - fullProfile.
// Учтите, что некоторые поля в объектах могут повторяться. В таком случае в результирующем объекте должно сохраниться значение, которое было получено позже (например, значение из 2020 более приоритетное по сравнению с 2019).
// Напишите код, который составит полное досье о возможной личности Сатоши Накамото. Изменять объекты satoshi2018, satoshi2019, satoshi2020 нельзя.
const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
        lat: 38.869422,
        lng: 139.876632
    }
}
const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
}
const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto',
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
}

//===== мой код =======
const satoshiFullProfile = ({...satoshi2018, ...satoshi2019, ...satoshi2020});
console.log('TASK 4 >>> Satoshi Full Profile :', satoshiFullProfile);



// ______________Задание 5____________________________________________
//Дан массив книг. Вам нужно добавить в него еще одну книгу, не изменяя существующий массив
// (в результате операции должен быть создан новый массив).
const books = [{
  name: 'Harry Potter',
  author: 'J.K. Rowling'
}, {
  name: 'Lord of the rings',
  author: 'J.R.R. Tolkien'
}, {
  name: 'The witcher',
  author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
  name: 'Game of thrones',
  author: 'George R. R. Martin'
}

//===== мой код =======
const newBooksarray = [...books, ({...bookToAdd})];
console.log('TASK 5 >>> new Books array : ', newBooksarray );



// ______________Задание 6____________________________________________
//Дан обьект employee. Добавьте в него свойства age и salary, не изменяя изначальный объект (должен быть создан новый объект,
// который будет включать все необходимые свойства). Выведите новосозданный объект в консоль.
const employee = {
  name: 'Vitalii',
  surname: 'Klichko'
}

//===== мой код =======
const addedSomeKeysEmployee = ({...employee, age: 35, salary: 12000});
console.log('TASK 6 >>> new object Employee :', addedSomeKeysEmployee );




// ______________Задание 7____________________________________________
//Дополните код так, чтоб он был рабочим
const array = ['value', () => 'showValue'];

//===== мой код =======
const [value, showValue] = array;

// alert(value); // должно быть выведено 'value'
// alert(showValue());  // должно быть выведено 'showValue'

console.log('TASK 7 >>> value :', value + '  / showValue() :', showValue());












