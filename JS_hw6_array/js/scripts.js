// Теоретический вопрос
//Опишите своими словами как работает цикл forEach.
//
// Задание
// Реализовать функцию фильтра массива по указанному типу данных.
// Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
// Написать функцию filterBy(), которая будет принимать в себя 2 аргумента.
// Первый аргумент - массив, который будет содержать в себе любые данные, второй аргумент - тип данных.
// - Функция должна вернуть новый массив, который будет содержать в себе все данные,
//   которые были переданы в аргумент, за исключением тех, тип которых был передан вторым аргументом.
//   То есть, если передать массив ['hello', 'world', 23, '23', null], и вторым аргументом передать 'string', то функция вернет массив [23, null].
//
// Литература:
//Массивы с числовыми индексами
// Массив: перебирающие методы
// Шесть типов данных, typeof



let arrayEl = [undefined,'hello', 27, 0, 'number', true, false, 'Infinity - бесконечность', 'NaN - ошибка вычислений', BigInt, '24712323134', 'string', 'boolean', true, false, 'true - истина',
    'false - ложь', 'null', undefined, 'имеет смысл «ничего» или «значение неизвестно»', undefined, 'переменная объявлена, но в неё ничего не записано', 299, 45,
    189, Symbol, 'Создаются новые символы с помощью функции Symbol()', 'cимволы гарантированно уникальны', 'Первые 7 типов называют «примитивными»',
    'object', 'используется для коллекций данных и для объявления более сложных сущностей', 999, 875, 36, 'результатом typeof является строка, содержащая тип' ];

function GetType() {
    const arrayType = ['string', 'number', 'boolean', 'null', 'undefined', 'object', 'function'];
    const id = Math.floor(Math.random() * arrayType.length);
    return arrayType[id];
}
function filterBy(arrEl, typeEl) {
    console.log('массив без', typeEl);
    return arrEl.filter(function(element) {
            return (typeof element !== typeEl );
        });
}
console.log(filterBy(arrayEl, GetType()));