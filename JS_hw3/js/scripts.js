    // Задание
    // Реализовать функцию, которая будет производить математические операции
    // с введеными пользователем числами.
    // Задача должна быть реализована на языке javascript,
    // без использования фреймворков и сторонник библиотек (типа Jquery).
    //
    // Технические требования:
    // Считать с помощью модального окна браузера два числа.
    // Считать с помощью модального окна браузера математическую операцию,
    // которую нужно совершить. Сюда может быть введено +, -, *, /.
    // Создать функцию, в которую передать два значения и операцию.
    // Вывести в консоль результат выполнения функции.
    //
    // Необязательное задание продвинутой сложности:
    // После ввода данных добавить проверку их корректности.
    // Если пользователь не ввел числа, либо при вводе указал не числа,
    // - спросить оба числа заново (при этом значением по умолчанию
    // для каждой из переменных должна быть введенная ранее информация).
let firstNumber, secondNumber, operand;
function getNumber() {
    let number;
    do {
        number = prompt('Enter Number');               // console.log(number);  // console.log(Boolean(+number));
    }while(!Boolean(+number));
    return number;
}
function getOperand(){
    let operand, validOperand;
    do {
        validOperand = false;
        operand = prompt('Enter Operand   +   -   *   /');                               // console.log(operand);
        if (operand === '+' || operand === '-' || operand === '*' || operand === '/'){
            validOperand = true;
        }
    }while (!validOperand);          // }while (operand !== '+' || operand !== '-' || operand !== '*' || operand !== '/');
    return operand;
}
function Calculator(fNumb, sNumb, operation) {
        switch (operation) {
            case '+':
                return fNumb + sNumb;
            case '-':
                return fNumb - sNumb;
            case '*':
                return fNumb * sNumb;
            case '/':
                return fNumb / sNumb;
        }
}
firstNumber = getNumber();
operand = getOperand();
secondNumber = getNumber();
console.log(firstNumber, operand, secondNumber +' = '+ Calculator(firstNumber, secondNumber, operand));
