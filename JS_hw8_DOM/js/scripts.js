// Задание
// Создать поле для ввода цены с валидацией. Задача должна быть реализована на языке javascript,
// без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
//   — При загрузке страницы показать пользователю поле ввода (input) с надписью Price.
//     Это поле будет служить для ввода числовых значений
//
// Поведение поля должно быть следующим:
//    — При фокусе на поле ввода - у него должна появиться рамка зеленого цвета.
//      При потере фокуса она пропадает.
//
//    — Когда убран фокус с поля - его значение считывается,
//          над полем создается span,
//          в котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}.
//         — Рядом с ним должна быть кнопка с крестиком (X).
//      Значение внутри поля ввода окрашивается в зеленый цвет.
//
//    — При нажатии на Х - span с текстом и кнопка X должны быть удалены.
//    — Значение, введенное в поле ввода, обнуляется.
//    — Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под полем выводить фразу - Please enter correct price. span со значением при этом не создается.
//
// В папке img лежат примеры реализации поля ввода и создающегося span.
// Литература:
// Поиск DOM элементов
// Добавление и удаление узлов
// Введение в браузерные события

let priceInput = document.getElementById('enteredPrice');

priceInput.addEventListener("blur", () => onBlur());
priceInput.addEventListener("focus", () => onFocus());

function onBlur() {
    if(priceInput.value <= 0){
        document.getElementById('containerForPrice').insertAdjacentHTML('afterbegin',
            `<div class="showPrice"><span>Correct price, pls!</span></div>`);
        priceInput.classList.add('error');
        return;
    }
    priceInput.classList.add('blur');

    document.getElementById('containerForPrice').insertAdjacentHTML('afterbegin', `
    <div class="showPrice"><span>Current price is:  ${priceInput.value} $ </span>
    <button class="deletePriceBtn">X</button></div>`);

    let price = document.querySelector('.showPrice');
    // let btn = document.querySelector('.deletePriceBtn');   // console.log(btn);
    document.querySelector('.deletePriceBtn').addEventListener("click",function() {
        price.remove();
        priceInput.value = '';
    });
}
function onFocus() {
    priceInput.classList.add('focus');

    if(priceInput.classList.contains('error')){
        priceInput.classList.remove('error');
        document.querySelector('.showPrice').remove();
    }
    if(priceInput.classList.contains('blur')){
        priceInput.classList.remove('blur');
    }
}

















// function changeStyle(n) {
//     if(priceInput.classList.contains(`${n}`)){
//         priceInput.classList.remove(`${n}`);
//     }
//     priceInput.classList.add(`${n}`);
// }









