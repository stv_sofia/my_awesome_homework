// Задание
//  Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем.
//  Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
// Технические требования:
//  Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser())
//  и дополните ее следующим функционалом:
//  - При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy)
//  и сохранить ее в поле birthday.
//  - Создать метод getAge() который будет возвращать сколько пользователю лет.
//  - Создать метод getPassword(), который будет возвращать
//         -первую букву имени пользователя в верхнем регистре,
//         +соединенную с фамилией (в нижнем регистре)
//         +и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).
// Вывести в консоль результат работы функции createNewUser(),
// а также функций getAge() и getPassword() созданного объекта.
function getData(question) {
    let entrdTxt;
    do {
        entrdTxt = prompt(question);
    } while (!entrdTxt || !isNaN(entrdTxt));
    return entrdTxt;
}
function getBirthDate(question){
    let entrdTxt, day, month, year, id, birthD;   // entrTxt = '02.12.1988';
    do {
        entrdTxt = prompt(question);
    } while (!entrdTxt);
    id = entrdTxt.indexOf("\.");
    day = entrdTxt.slice(0, id);
    entrdTxt = entrdTxt.slice(id+1);

    id = entrdTxt.indexOf("\.");
    month = entrdTxt.slice(0, id);
    entrdTxt = entrdTxt.slice(id+1);

    year = entrdTxt;
    return new Date(`${year}-${month}-${day}`); // return '02.12.1988';
}
function createNewUser(firstName = 'user', lastName = 'user', birthdayDate = '2000-01-01') {    // берет значения по умодчанию если undefined
    return {
        firstName,      // firstName: firstName
        lastName,
        birthdayDate,
        getLogin: function () {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        age: function () {
            let birthDay = new Date(this.birthdayDate);
            let toDay = new Date();
            return Math.floor((toDay - birthDay)/ 1000 /60 /60 / 24/ 365);
        },
        getPassword: function () {
            let birthDay = new Date(this.birthdayDate);
            return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + birthDay.getFullYear()) ;
        }
    };
}
const user = createNewUser(getData('Введите имя'), getData('Введите фамилия'), getBirthDate('Введите дату рождения в формате dd.mm.yyyy'));
console.log(user);
console.log('логин:',user.getLogin());
console.log('дата рождения:', user.birthdayDate);
console.log('полных лет:', user.age());
console.log('пароль:',user.getPassword());
