// Задание
// Реализовать переключение вкладок (табы) на чистом Javascript.
//
//Технические требования:
//     В папке tabs лежит разметка для вкладок.
//     — Нужно, чтобы по нажатию на вкладку отображался конкретный текст для нужной вкладки.
//     — При этом остальной текст должен быть скрыт.
//           В комментариях указано, какой текст должен отображаться для какой вкладки.
//
//     — Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
//
//     — Нужно предусмотреть, что текст на вкладках может меняться,
//     и что вкладки могут добавляться и удаляться. При этом нужно, чтобы функция,
//     написанная в джаваскрипте, из-за таких правок не переставала работать.
//     Литература:
// HTMLElement.dataset
document.addEventListener('DOMContentLoaded', onReady);
function onReady (){
let tab = function(){
    let tabButton = document.querySelectorAll('.tabs-title'),
        tabContent = document.querySelectorAll('.tab'),
        tabNameActiveBtn;

    console.log(tabButton);
    tabButton.forEach(item => {
        item.addEventListener('click', defineActiveTab)
    });

    function defineActiveTab() {
        tabButton.forEach(item => {
            item.classList.remove('is-active');
        });
        // console.log(this);
        this.classList.add('is-active');
        tabNameActiveBtn = this.getAttribute('data-tab-name');
        // console.log(tabNameActiveBtn);
        defineTabContent(tabNameActiveBtn);
    }

    function defineTabContent(tabName) {
        // console.log(tabName);
        tabContent.forEach(item => {
            // if (item.classList.contains(tabName)){
            //     item.classList.add('is-active');
            // } else {
            //     item.classList.remove('is-active');
            // }
            item.classList.contains(tabName) ? item.classList.add('is-active') : item.classList.remove('is-active');
        });
    }
}

tab();

}