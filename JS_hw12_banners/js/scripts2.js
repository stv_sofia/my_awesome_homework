document.addEventListener("DOMContentLoaded", onReady);
function onReady() {
    let activeIndex, btnActive, idTimer1 , idTimer2, delayId1, delayId2;
    const slides = document.getElementById('slides');                                                   // console.log(slides);
    const slidesInner = slides.querySelector('.slides-inner');                                          // console.log(slidesInner);
    const images = slides.querySelectorAll('img');                                                      // console.log(images);
    const slideWidth = slides.clientWidth;
    const timeDelay = 10000;

    document.querySelector('.btn').insertAdjacentHTML('beforebegin',
        `<button  id="timer" class="btn">${timeDelay/1000}</button>`);

    slides.addEventListener('click', function (event) {
        btnActive = event.target;
        if(btnActive.id === 'stop'){
            clearIdTimeout(idTimer1, idTimer2);
            clearIdTimeout(delayId1, delayId2);
            document.getElementById('timer').innerHTML = 'stopped';
            images[activeIndex].classList.toggle('active-img');                                             console.log('btnStop activeIndex: ', activeIndex);
        }
        if (btnActive.id === 'run') {                                                                              console.log('btnRun activeIndex: ', activeIndex);
            makeSlidesShow(activeIndex);
        }
    });

    function clearIdTimeout(id1, id2) {
        clearTimeout(id1);                                                                                       // console.log("Таймер остановлен")
        clearTimeout(id2);
    }

    function setTimer(i) {
        idTimer1 = setTimeout ( function start() {
            if (i <= 0) return;
            // вывод оформленн с учетом timeDelay/1000 = 10
            (i < timeDelay/1000) ? document.querySelector('#timer').innerHTML = `0${i}` : document.querySelector('#timer').innerHTML = `${i}`;
            i--;
            idTimer2 = setTimeout(start, 1000);
        },0);
    }

    function slide(transition, transform){
        slidesInner.style.transition = transition;
        slidesInner.style.transform = transform;
    }

    function makeSlidesShow() {
        clearIdTimeout(idTimer1, idTimer2);
        setTimer(timeDelay/1000);

        // if(slides.querySelector('.active-img')){      //  выходит за пределы массива катинок ???
        //     activeIndex = slides.querySelector('.active-img').getAttribute('data-ind');                          console.log('from start function activeIndex: ', activeIndex);
        //     images[activeIndex].classList.toggle('active-img');}

        for(let i = 0; i < images.length; i++){       //  как вариант
            if(images[i].classList.contains('active-img')){
                activeIndex = i;
                images[i].classList.toggle('active-img');
            }
        }                                                                                                           console.log('from start function activeIndex: ', activeIndex);

        delayId1 = setTimeout(function run() {
            if(activeIndex + 1 === images.length) {
                activeIndex = 0;
                slide('none', `translate3d(${activeIndex * -slideWidth}px, 0, 0)`);
                setTimeout(run, 0);
            } else {
                clearIdTimeout(idTimer1, idTimer2);
                setTimer(timeDelay/1000);
                activeIndex ++;
                slide('0.5s ease-in-out',`translate3d(${activeIndex * -slideWidth}px, 0, 0)`);
                delayId2 = setTimeout(run, timeDelay);
            }
        },timeDelay);
    }

    makeSlidesShow();
}