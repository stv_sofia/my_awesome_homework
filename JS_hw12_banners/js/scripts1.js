document.addEventListener("DOMContentLoaded", onReady);
function onReady() {
    let activeIndex, btnActive, delayId1, delayId2;
    const timeDelay = 3000;
    document.getElementById('slides').addEventListener('click', function (event) {
        btnActive = event.target;
        if (btnActive.id === 'run') {                                                                                 console.log('btnRun activeIndex: ', activeIndex);
            makeSlidesShow();}
    });

    function makeSlidesShow() {
        const slides = document.getElementById('slides');
        const slideWidth = slides.clientWidth;
        const slidesInner = slides.querySelector('.slides-inner');
        const images = slides.querySelectorAll('img');

        // if(slides.querySelector('.active-img')){      //  выходит за пределы массива катинок ???
        //     activeIndex = slides.querySelector('.active-img').getAttribute('data-ind');                          console.log('from start function activeIndex: ', activeIndex);
        //     images[activeIndex].classList.toggle('active-img');}

        for(let i = 0; i < images.length; i++){       //  как вариант
            if(images[i].classList.contains('active-img')){
                activeIndex = i;
                images[i].classList.toggle('active-img');
            }
        }                                                                                                          console.log('from start function activeIndex: ', activeIndex);

        function slide(transition, transform){
            slidesInner.style.transition = transition;
            slidesInner.style.transform = transform;
        }

        delayId1 = setTimeout(function run() {
            if(btnActive && btnActive.id === 'stop') {                                                                console.log('btnStop activeIndex: ', activeIndex);
                images[activeIndex].classList.toggle('active-img');
                clearTimeout(delayId1);
                clearTimeout(delayId2);
                return;
            }
            if(activeIndex + 1 === images.length) {
                activeIndex = 0;
                slide('none', `translate3d(${activeIndex * -slideWidth}px, 0, 0)`);
                setTimeout(run, 0);
                } else {
                    activeIndex ++;
                    slide('0.5s ease-in-out',`translate3d(${activeIndex * -slideWidth}px, 0, 0)`);
                    delayId2 = setTimeout(run, timeDelay);
                }
            }, timeDelay);
    }

    makeSlidesShow();
}