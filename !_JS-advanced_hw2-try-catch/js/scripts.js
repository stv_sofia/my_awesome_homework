// Дан массив books.
// — Выведите этот массив на экран в виде списка
//  (тег ul - список должен быть сгенерирован с помощью Javascript).
// — На странице должен находиться div с id="root", куда и нужно будет положить этот список
//  (похожая задача была дана в модуле basic).
// — Перед выводом обьекта на странице, нужно проверить его на корректность
//  (в объекте должны содержаться все три свойства - author, name, price).
// — Если какого-то из этих свойств нету, в консоли должна высветиться ошибка
//   с указанием - какого свойства нету в обьекте. Те элементы массива,
//   которые являются некорректными по условиям предыдущего пункта, не должны появиться на странице.
const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

function createElementUl(array) {
    const filledBooksArray = array.map( (item, index) => {
    try{
        if (!item.author) {
            throw new SyntaxError(`Данные неполны: нет имени автора ${index}-ой книги`);
        }
        if (!item.name) {
            throw new SyntaxError(`Данные неполны: нет названия ${index}-ой книги`);
        }
        if (!item.price) {
            throw new SyntaxError(`Данные неполны: нет стоимости ${index}-ой книги`);
        }
        } catch (err) {
            if (err.name === "SyntaxError") {
                console.log( `${err.stack}  |  Details: ${err.message}` );
            } else {
                throw err; // проброс
            }
        } finally {
            if (item.author && item.name && item.price) {
                return `<li>Автор: ${item.author} | Название: "${item.name}" | Стоимость: ${item.price}грн.</li>`;
            }
        }
    }).filter(Boolean);       // .filter((el) => el)  // .filter(function(el){return el})
    // console.log('Full Filled Books Array : ', filledBooksArray);
    const strHtml = `<ul>Книги c полными данными: ${filledBooksArray.join('')}</ul>`;          // console.log(strHtml);
    document.querySelector('#root').insertAdjacentHTML("afterbegin", strHtml);
}
createElementUl(books);