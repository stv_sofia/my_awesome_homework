// Задание
//     Реализовать простую программу на Javascript, которая будет взаимодействовать с пользователем
//     с помощью модальных окон браузера - alert, prompt, confirm.
//     Задача должна быть реализована на языке javascript,
//     без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
//     Считать с помощью модельного окна браузера данные пользователя: имя и возраст.
//     Если возраст меньше 18 лет - показать на экране сообщение: You are not allowed to visit this website.
//     Если возраст от 18 до 22 лет (включительно) - показать окно
//     со следующим сообщением: Are you sure you want to continue? и кнопками Ok, Cancel.
//     Если пользователь нажал Ok, показать на экране сообщение: Welcome,  + имя пользователя.
//     Если пользователь нажал Cancel, показать на экране сообщение: You are not allowed to visit this website.
//     Если возраст больше 22 лет - показать на экране сообщение: Welcome,  + имя пользователя.
//     Обязательно необходимо использовать синтаксис ES6 (ES2015) при создании переменных.
//
// Необязательное задание продвинутой сложности:
//     После ввода данных добавить проверку их крректности. Если пользователь не ввел имя,
//     либо при вводе возраста указал не число - спросить имя и возраст заново
//     (при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация).
let name, age;
let count = 0;    // подсчет Сancel - можно без него (в половину код сократится) - но а вдруг пользователь захочет уйти
let eXit = false;
let confContinueToEnter = false;
alert('----------------  TASK #1   -------------- \n' +
    ' Fill in the Name and Age fields to Enter our resource! \n' +
    ' - Please, Enter your Name \n' +
    ' (letters or letters+digit | do not start with digit), \n ' +
    ' - Enter your Age (!only digit (maximum 99)');
do {
    name = prompt('Enter your NAME    /   do not start with digit, pls'); // ввод имени
    // console.log(name); // console.log(parseInt(name));
    if(name === null && count<4) {    // подсчет отмен ввода данных
        count++;
    }
    if(count === 3) {                 // три раза Сancel --> выход из программы ввода данных
        alert('Have a good day. Goodbye.');
        eXit = true;
        break;
    }
} while (name === null || name === '' || !isNaN(name) || parseInt(name)); // проверка
                                                                //  || parseInt(name) === NaN   - если число есть в строке но после буквы - не работает - не проверяет(( ???
if(!eXit){    // если не вышли из программы (т е не отменили ввод данных)
    count = 0;
    do {
        age = prompt('Enter your AGE    /   a natural Number (maximum 99), pls');  // ввод возраста
        // console.log(age);
        if(age === null && count<4) {  // подсчет отмен ввода данных
            count++;
        }
        if(count === 3) {              // три раза Сancel --> выход из программы ввода данных
            alert('Have a good day. Goodbye.');
            eXit = true;
            break;
        }
    } while (+age === null || isNaN(+age) || +age <= 0 || +age >= 99);
}
if(!eXit){   // если не вышли из программы (т е не отменили ввод данных)
    if(age < 18){
        alert('You are not allowed to visit this website. Have a good day. Goodbye.'); // слишком молод
    } else if ( age >= 18 && age <= 22) {
        confContinueToEnter = confirm('Are you sure you want to continue?');  // норм, но поддтверди намерение зайти))
            if (confContinueToEnter) {
                alert('Welcome, ' + name + '!');  // после подверждения - привет)
            } else {
                alert('Have a good day. Goodbye.');  // после НЕподверждения)) - пока)
            }
    } else {         // иначе - привет)
        alert('Welcome, ' + name + '!');
    }
}




























// let name;
// let age;
// let flag;
// let confTryAgain;
// let goToCheckAge = false;
// let eXit = false;
// alert('----------------  TASK #1   -------------- \n' +
//     ' Fill in the Name and Age fields to Enter our resource! \n' +
//     ' Please, Enter your Name in letters, Age in numbers.');
// do{
//     flag = true;
//     // console.log(flag);
//     name = prompt('Enter your NAME    /   no digit symbols, pls');
//     if(name===null || name === '' || !isNaN(name)){
//         alert('You have Canceled your registration \nOR entered Empty string OR Number.');
//         flag = false;
//         // console.log(flag);
//     } else {
//         age = + prompt('Enter your AGE    /   a natural Number, pls, Only');
//         // console.log('age = ' + age);
//         if( age===null || isNaN(age) || age <= 0){
//             alert('You have Canceled your registration \nOR entered Not a Number.');
//             flag = false;
//             // console.log(flag);
//         } else {
//             goToCheckAge = true;
//             // console.log(flag);
//             break;
//         }
//     }
//     if(flag === false){
//         confTryAgain  = confirm('Do you want to Try filling the form Again?');
//         if(confTryAgain === false){
//             alert('Have a good day. Goodbye.');
//             eXit = true;
//         }
//     }
//
// }while (eXit === false);
//
// let confContinueToEnter = false;
// if(goToCheckAge === true){
//     if(age < 18){
//         alert('You are not allowed to visit this website. Have a good day. Goodbye.');
//     } else if(age >= 22) {
//         alert('Welcome, ' + name + '!');
//     } else if ( age >= 18 && age < 22) {
//         confContinueToEnter = confirm('Are you sure you want to continue?');
//         if (confContinueToEnter) {
//             alert('Welcome, ' + name + '!');
//         } else {
//             alert('Have a good day. Goodbye.');
//         }
//     }
// }