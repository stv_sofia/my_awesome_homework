// document.addEventListener('DOMContentLoaded', function() {
//     console.log('Document started from old way syntax!!!');
// });

$(document).ready(function() {                                                      // console.log('Document is ready to use!!!');

    $(".navigation li a").on("click", function (event) {
        event.preventDefault();                                                    // let goToId = $(this).attr("href"),  //     topOffset = $(goToId).offset().top;    // $("html, body").animate({scrollTop:topOffset}, 1500);
        $("html, body").animate({scrollTop:$($(this).attr("href")).offset().top}, 1000);
    });

    const $btnToTop = $(".to-top");
    $(window).on("scroll", function () {
        if($(window).scrollTop() >= $(window).height()){
            $btnToTop.fadeIn();
        } else {
            $btnToTop.fadeOut();
        }
    });

    $btnToTop.on("click", function () {
        $("html, body").animate({scrollTop:0}, 1000);
    });

    $(".to-slide-toggle").click(function () {       // console.log($(this)[0]);
        // console.log(`.${(this).nextElementSibling.className}`);
        $(`.${(this).nextElementSibling.className}`).slideToggle(1500);
        ($(".fa-chevron-up")).toggleClass('fa-chevron-down');
    });

});