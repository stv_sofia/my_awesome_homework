document.addEventListener("DOMContentLoaded", function () {

// ---------------------Our Services ----- tabs --------------------------------------------------------
    document.querySelector(".service-tab-header").addEventListener('click', function (event) {
        let tab = event.target;  // console.log(tab);
        let id = tab.getAttribute('data-tab');
        let content = document.querySelector(`.js-tab-content[data-tab="${id}"`);  // console.log(content);
        let arrow = document.querySelector(`i.fa-caret-down[data-tab="${id}"`);  // console.log(arrow);

        document.querySelector('.js-tab-trigger.active').classList.remove('active');
        tab.classList.add('active');

        document.querySelector('i.fa-caret-down.active').classList.remove('active');
        arrow.classList.add('active');

        document.querySelector('.js-tab-content.active').classList.remove('active');
        content.classList.add('active');
    });


//------------------------- OurWorks Section --- tabs ------- &  Sorting by tabs ------------------------------------
    let arrayImgAll = [
        {src: 'imgs/ourWorkImg/graphic-design1.jpg', filter: 'graphicDesign'},
        {src: 'imgs/ourWorkImg/landing-page1.jpg', filter: 'landingPage'},
        {src: 'imgs/ourWorkImg/web-design6.jpg', filter: 'webDesign'},
        {src: 'imgs/ourWorkImg/wordpress8.jpg', filter: 'wordpress'},

        {src: 'imgs/ourWorkImg/graphic-design8.jpg', filter: 'graphicDesign'},
        {src: 'imgs/ourWorkImg/landing-page3.jpg', filter: 'landingPage'},
        {src: 'imgs/ourWorkImg/web-design2.jpg', filter: 'webDesign'},
        {src: 'imgs/ourWorkImg/wordpress7.jpg', filter: 'wordpress'},

        {src: 'imgs/ourWorkImg/graphic-design5.jpg', filter: 'graphicDesign'},
        {src: 'imgs/ourWorkImg/landing-page2.jpg', filter: 'landingPage'},
        {src: 'imgs/ourWorkImg/web-design3.jpg', filter: 'webDesign'},
        {src: 'imgs/ourWorkImg/wordpress6.jpg', filter: 'wordpress'},

        {src: 'imgs/ourWorkImg/graphic-design12.jpg', filter: 'graphicDesign'},
        {src: 'imgs/ourWorkImg/landing-page7.jpg', filter: 'landingPage'},
        {src: 'imgs/ourWorkImg/web-design5.jpg', filter: 'webDesign'},
        {src: 'imgs/ourWorkImg/wordpress10.jpg', filter: 'wordpress'},

        {src: 'imgs/ourWorkImg/graphic-design7.jpg', filter: 'graphicDesign'},
        {src: 'imgs/ourWorkImg/landing-page6.jpg', filter: 'landingPage'},
        {src: 'imgs/ourWorkImg/web-design1.jpg', filter: 'webDesign'},
        {src: 'imgs/ourWorkImg/wordpress1.jpg', filter: 'wordpress'},

        {src: 'imgs/ourWorkImg/graphic-design6.jpg', filter: 'graphicDesign'},
        {src: 'imgs/ourWorkImg/landing-page5.jpg', filter: 'landingPage'},
        {src: 'imgs/ourWorkImg/web-design4.jpg', filter: 'webDesign'},
        {src: 'imgs/ourWorkImg/wordpress4.jpg', filter: 'wordpress'},

        {src: 'imgs/ourWorkImg/graphic-design9.jpg', filter: 'graphicDesign'},
        {src: 'imgs/ourWorkImg/landing-page8.png', filter: 'landingPage'},
        {src: 'imgs/ourWorkImg/web-design7.jpg', filter: 'webDesign'},
        {src: 'imgs/ourWorkImg/wordpress5.jpg', filter: 'wordpress'},

        {src: 'imgs/ourWorkImg/graphic-design10.jpg', filter: 'graphicDesign'},
        {src: 'imgs/ourWorkImg/landing-page4.jpg', filter: 'landingPage'},
        {src: 'imgs/ourWorkImg/web-design8.png', filter: 'webDesign'},
        {src: 'imgs/ourWorkImg/wordpress2.jpg', filter: 'wordpress'},
    ];

    document.querySelector(".work-tabs-container").addEventListener('click', function (event) {
        let tab = event.target,
            id = tab.getAttribute('data-tab'),
            btn = document.querySelector('.load-more-works');
        (id === '1') ? btn.classList.remove('not-active') : btn.classList.add('not-active');

        document.querySelector('.work-tab-active').classList.remove('work-tab-active');
        tab.classList.add('work-tab-active');

        let contentTabActive = tab.textContent;        // console.log(contentTabActive);
        const slidesHover = document.querySelectorAll('.workTabActiveContent');
        for(let item of slidesHover){
            item.textContent = contentTabActive;       // console.log(item.textContent);
        }
        showSelectedWorks(tab, id);
    });

    function  showSelectedWorks(tabActive, idActive){
        let filter = tabActive.getAttribute('data-filter'),
            arrayBoxImg = document.querySelectorAll('.work-img-box'),
            arrayImg = document.querySelectorAll('.work-img'),
            filterImg;                                   // console.log('tab-filter: ' + filter);
        if(idActive === '1'){
            for(let i = 0; i < arrayBoxImg.length; i++){
                arrayBoxImg[i].classList.remove('hide');
            }
        } else {
            for(let i = 0; i < arrayBoxImg.length; i++){
                filterImg = arrayImg[i].getAttribute('data-filter');     // console.log('img-filter: ' + filterImg);
                if(filterImg !== filter){
                    arrayBoxImg[i].classList.add('hide');
                } else { arrayBoxImg[i].classList.remove('hide'); }
            }
        }
    }
                      // -- OurWorks Section --- Load More ourWorks --
    const btnLoadWork = document.getElementById('loadMoreWork');
    btnLoadWork.addEventListener('click', () => {
        let contentTabActive = document.querySelector('.work-tab-active').textContent,
            strHtmlInsert = '';
        for (let i = 16; i < 32; i++){
            strHtmlInsert += `<div class="work-img-box">
                                    <img data-filter="${arrayImgAll[i].filter}" class="work-img" src = "${arrayImgAll[i].src}" alt="our work image"/>
                                    <div class="work-img-selected-by-tab">
                                        <img src="imgs/shapes/icon.png" alt="img"/>
                                        <span>creative design</span>
                                        <span class="workTabActiveContent">${contentTabActive}</span>
                                    </div>
                                </div>`;                                              //console.log(`<img src = "${arrayImgAll[i].src}"/>`);
        }
        document.querySelector('.work-img-container').insertAdjacentHTML('beforeend', strHtmlInsert);
        document.querySelector('.load-more-works').classList.add('display-none');
    });




// ----------------------- Feedback Section --- слайдер фоток --- slider --- tabs -------------------------------------------------------
    let activeBtn, directionBtn, activeIndex = 0;
    const slideContainer = document.querySelector('.feedback-slides'),
          slidePhotoItem = document.querySelectorAll('.feedback-photo-item img'),
          slidePhotoBox = document.querySelectorAll('.feedback-photo-item'),
          slideWidth = slidePhotoBox[1].clientWidth + 3;

                     //-- смена активного эл-та слайдера по клику кнопок next, prev --
    document.querySelector('.btn-container').addEventListener('click', function (event) {
        activeBtn = event.target;
        directionBtn = activeBtn.getAttribute('data-id');
        (directionBtn === 'next') ? nextSlide() : prevSlide();

                     //-- смены всех связанных по клику кнопок next, prev --
        let dataTabActive = slideContainer.querySelector('img.active-tab').getAttribute('data-tab');
        (directionBtn === 'next' && dataTabActive < 8) ? dataTabActive++ : false;
        (directionBtn === 'prev' && dataTabActive > 1) ? dataTabActive-- : false;
        // console.log(dataTabActive);
        let photoActive = slidePhotoItem[dataTabActive-1];

        dataTabActive = changeActivePhoto(photoActive);
        showActiveDetales(dataTabActive);
    });

    function slideStep(activeIndex) {
        slideContainer.style.transition ='transform 1s ease-in-out';
        slideContainer.style.transform = `translate3d(${activeIndex * -slideWidth}px, 0, 0)`;
    }
    function nextSlide() {
        (activeIndex === 4) ? false : slideStep(++activeIndex);
        btnNoMoreSlide(activeIndex);

    }
    function prevSlide() {
        (activeIndex === 0) ? false : slideStep(--activeIndex);
        btnNoMoreSlide(activeIndex);
    }
                          //-- ф-ия смены стилей (цвета) кнопок next, prev --
    function btnNoMoreSlide(activeIndex) {
        let btns = document.querySelectorAll('.btn-container i');
        for(let item of btns){ item.classList.remove('not-active-btn'); }

        if(activeIndex === 4 && directionBtn === 'next'){ activeBtn.classList.add('not-active-btn');}
        if(activeIndex === 0 && directionBtn === 'prev'){ activeBtn.classList.add('not-active-btn');}
    }
                               //-- смена активного эл-та слайдера по клику на фото --
    slideContainer.addEventListener('click', function (e) {
     let photoTabActive = e.target;
     if(photoTabActive.tagName === 'IMG'){
         let dataTabActive = changeActivePhoto(photoTabActive);
         showActiveDetales(dataTabActive);
        }
    });
                               //-- ф-ия смены активной фотки по активному дата-таб --
    function changeActivePhoto(photoTabActive) {
        slideContainer.querySelector('img.active-tab').classList.remove('active-tab');
        photoTabActive.classList.add('active-tab');
        return photoTabActive.getAttribute('data-tab');
    }
                              //-- ф-ия смены всех связанных эл-ов по активному дата-таб --
    function showActiveDetales(dataTabActive) {
        let photoActive = slidePhotoItem[dataTabActive-1];

        let srcActive = photoActive.getAttribute('src');
        document.querySelector('.photo-selected img').setAttribute('src', srcActive);

        document.querySelector('.feedback-quot.active-quot').classList.remove('active-quot');
        let quots = document.querySelectorAll('p.feedback-quot');
        quots[dataTabActive-1].classList.add('active-quot');

        document.querySelector('span.name-active').innerHTML = photoActive.getAttribute('data-name');
    }

});




//----------jQuery---------- Gallery Section ------ masonry---- masonry ---- masonry ----------------------------------------
    $(document).ready(function() {
        const masonryGrid = $(".gallery-img-container");
        function newMasonry(){
            $(masonryGrid).imagesLoaded(function () {
                $(".gallery-img-container").masonry({    // применяем плагин masonry к контейнеру с картинками
                    itemSelector: '.item',
                    columnWidth: 373,
                    gutter: 20
                });
            });
        }
        newMasonry();
        $("#loadMoreGallery").on("click", function () {
            let imgArray = $(".gallery-img-container .item"),
                lastItemId = imgArray.length,
                strHtml = '';
            if (lastItemId === 12) $(".load-more-gallery").addClass('display-none');
            if (lastItemId < 18){       // добавляем 2 раза по 6 картинок к уже имеющимся шести
                // console.log(lastItemId);
                for (let i = lastItemId; i < lastItemId + 6; i++){
                    strHtml += `<div class="item"><img src="imgs/gallery/p${i+1}.jpg" alt="image of gallery"/></div>`;
                    // console.log(`<div class="item"><img src="imgs/gallery/p${i+1}.jpg" alt="image of gallery"/></div>`);
                    }
                $(masonryGrid).append(strHtml);
                $(masonryGrid).masonry('reloadItems');
                $(masonryGrid).masonry('layout');
                newMasonry();
            }
        });
    });














